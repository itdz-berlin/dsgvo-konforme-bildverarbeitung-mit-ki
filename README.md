<div align="center">
  <img src="images/Logos_collage.PNG" alt="Logos der drei Projektpartner">
</div>
<div align="center">
  <img src="images/TXL_Drohne.jpg" alt="Drohne vor dem ehemaligen Flughafengebäude Berlin TXL">
  </div>
<div align="center">
  Foto: Axel Dannbauer/CADdy Geomatics GmbH
</div>


## Die aktuelle Situation 

- Am ehemaligen Flughafen Berlin TXL werden regelmäßig hochauflösende Drohnenaufnahmen gemacht

## Problem 

- BTXL-eigene Drohnenbilder werden zahlreichen Stakeholdern zur Verfügung gestellt
- Diese beinhalten personenbezogene Daten wie Fahrzeuge und Menschen

## Lösung

- Erkennung von personenbezogenen Daten in den Luftaufnahmen mittels KI
- Anonymisierung (Blurring) von personenbezogenen Daten in den Luftaufnahmen
- Projekt auf Open CoDE verfügbar

## Ausblick

- Ggf. „Löschen“ von Autos, Bussen, etc. statt Blurring
- Übertragung und Skalierung in weitere Anwendungsfelder (Facility Management, Feuerwehr, Grünflächenmanagement, etc.)


## Haupt-Repository

Dies ist das Haupt-Repository des Projekts "DSGVO-konforme Bildverarbeitung mit KI"  zur Verarbeitung von Luftaufnahmen.
Die eigentlichen Dateien (z.B. Skripte, Beispieldaten/Bilder, Gewichte-Datei) befinden si


## Installation und Verwendung

Anweisungen zur Installation und Verwendung der einzelnen Skripte finden Sie in den jeweiligen Unterverzeichnissen:

- **ai-training**
- **scripts**

Jedes dieser Skripte enthält eine eigene README.md, in der alle notwendigen Schritte beschrieben sind.

# Kontakt

Innovationsmanagement:<br> 
innovation@itdz-berlin.de

IT-Dienstleistungszentrum Berlin
<br> Anstalt des öffentlichen Rechts
<br> Berliner Straße 112-115
<br> 10713 Berlin

Wir freuen uns auf den Austausch.
