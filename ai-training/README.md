# Berlin-TXL Training - Manual

This is the README/Manual to use the trained weights and the Yolov4 network to make predictions - draw bounding boxes, blurr, create masks or .json files.


## Prerequisites

- Ubuntu 20.04.5 LTS "focal" (Server, OS or Virtual Machine)
- Docker


## Installation

### Docker
Docker is used for containerizing the training, as well as the inference (using the weights to draw bounding boxes etc.).

#### Download & Installation
For installation please refer to the offical website: [Docker.com/install](https://docs.docker.com/engine/install/ubuntu/)

#### Setup docker container

1. Change directory into:
- /docker

2. Check completeness of the files:

- Dockerfile
- prerequisites.sh


3. Build docker image.
```bash
# check if prereqisites.sh is executable
ls -l

# (optional: make file executable)
chmod +x prerequisites.sh

# build container
docker build -t <image-name> path
```
Choose any name for 'image-name':
```bash
# Example:
docker build -t berlin-txl-training .
```

4. You can check, if the build was successfull with one of those commands:
```bash
docker image list                      # (optional)
docker image list | grep <image-name>  # (optional)
```

5. Run container

You can again choose any name for <any-name> to name the container.  '-p 8070:8070' defines
the ports, on which the container is reachable. <docker-image-name> should be the name, given before
in step 3.
One important options is to set the directories: <dir-to-add>:<where-to-add>. This allows you to add specific
directories, that are needed within yolo into the container.
To run a Docker Image there are different [options](https://docs.docker.com/engine/reference/commandline/run/).

```bash
docker run -v <dir-to-add>:<where-to-add> --shm-size 8 --name <any-name> --gpus all -it -p 8070:8070 <docker-image-name>

# Example:
docker run -v /<path-to-folder>/weights/:/darknet/weights -v /<path-to-folder>/config/:/darknet/config -v /<path-to-folder>/datasets/:/darknet/datasets -v /<path-to-folder>/trainings/:/darknet/trainings --shm-size 8 --name txl-training --gpus all -it -p 8060:8060 berlin-txl-training
```

After running the container, you can check with `docker ps -a` if the container is alive.


### Yolo v4 (Darknet)

YOLOv4 is a state-of-the-art object detection algorithm that was released in 2020. It stands for "You Only Look Once" version 4, and it uses a neural network architecture called Darknet to detect objects in images and video frames.
When setting up the docker container, yolov4 is automatically downloaded from github and installed inside the container.
No further steps are necessary!

## Usage

When we want to use our weights & YOLO to train on pictures,
we use the container in which everything is ready to use.

Before starting to train, the trainings set has to be devided into a training, and a validation set.
For this go to '/datasets' where you should copy a folder with trainings data (.txt/.img-pairs).
The provided script 'create_train_val.py' will create two files need for training:

- train.txt: all files used for training.
- val.txt: all files used for validating each training epoch and adjusting the weights.

Before using this script make sure to adapt the variables in the beginning:

- src_path
- dst_path
- path

An explanation is given within the file.
After adjusting these values the script can be used with:
```bash
python3 create_train_val.py
```

### Enter a running container:
```bash
docker container exec -it <name-of-container> bash

# Example
docker container exec -it txl-training bash
```

After enteringthe running container, the terminal change to something like this:
```bash
root@32a5c1afbb10:/darknet#
```

To start the training there is following basic command:
```bash

./darknet detector [train/test/valid/demo/map] [data] [cfg] [weights (optional)]

# Example:
./darknet detector train trainings/training-00/obj.data config/yolov4-custom.cfg weights/yolov4-training-09-best-weights-01-03-2023.weights -dont_show -mjpeg_port 8090 -map
```

Be carefull to adjust the parameters in this command wich each training.


### Leave a container without stopping it:

```bash
<strg + p> <strg + q>
```



## Contributing

[yolov4 - Offical github repo](https://github.com/AlexeyAB/darknet)
