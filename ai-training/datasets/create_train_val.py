# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


""" Create .txt file for training & validation. """


__version__ = "1.0"
__author__ = "klaus.moser@fujitsu.com, mohamed.belkhechine@fujitsu.com"


import os
import random


# TODO:
# Folder with the (new) trainigns data (.img/.txt pairs !!!!)
# Those are the (new) trainings files
src_path = '/home/grafitti/Projects/Berlin-TXL-Training/datasets/EXAMPLE-TRAINING-SET/'

# TODO:
# Folder to save the .txt files (this should be the trainings folder)
# Each trainng needs a 'train.txt' & 'val.txt' to determine which file to use
# for training or validation.
dst_path = '/home/grafitti/Projects/Berlin-TXL-Training/trainings/training-00/'

# TODO:
# Name of the path in the 'train.txt' & 'val.txt'
# (e.g. in this example 'EXAMPLE-TRAINING-SET')
path = '/darknet/datasets/EXAMPLE-TRAINING-SET/'

# TODO:
# Defines how much (in %) should be used as training-/validation data
# Default: 0.85 (85% training; 15% validation)
TEST_VAL_RATIO = 0.85

# List all files
files = [f for f in os.listdir(src_path) if os.path.isfile(os.path.join(src_path, f)) and not f.endswith('txt')]
imgs = []


for f in files:
    filename = os.path.basename(f)
    imgs.append(filename)


random.shuffle(imgs)
total_number_of_images = len(imgs)
train_number_of_images = int((TEST_VAL_RATIO) * total_number_of_images)


train_imgs = imgs[:train_number_of_images]
val_imgs = imgs[train_number_of_images:]


def create_file(dataset_type, imgs, path) -> None:
    """
    Create train.txt' & 'val.txt' files.

    :return
    """

    print(dataset_type)

    try:
        with open(dst_path + dataset_type, 'w') as f:
            for name in imgs:
                f.write(os.path.join(path, name) + '\n')
    except Exception as e:
        print(e)

# create files
create_file('train.txt', train_imgs, path)
create_file('val.txt', val_imgs, path)
