# Berlin-TXL Inference - Manual

This is the README/Manual to use the trained weights and the Yolov4 network to make predictions - draw bounding boxes, blurr, create masks or .json files.


## Prerequisites

- Ubuntu 20.04.5 LTS "focal" (Server, OS or Virtual Machine)
- Docker


## Installation

### Docker
Docker is used for containerizing the training, as well as the inference (using the weights to draw bounding boxes etc.).

#### Download & Installation
For installation please refer to the offical website: [Docker.com/install](https://docs.docker.com/engine/install/ubuntu/)

#### Setup docker container

1. Change directory into:
- /docker

2. Check completeness of the files:

- Dockerfile
- prerequisites.sh


3. Build docker image.
```bash
# check if prereqisites.sh is executable
ls -l

# (optional: make file executable)
chmod +x prerequisites.sh

# build container
docker build -t <image-name> path
```
Choose any name for 'image-name':
```bash
# Example:
docker build -t berlin-txl-inference .
```

4. You can check, if the build was successfull with one of those commands:
```bash
docker image list                      # (optional)
docker image list | grep <image-name>  # (optional)
```

5. Run container

You can again choose any name for <any-name> to name the container.  '-p 8070:8070' defines
the ports, on which the container is reachable. <docker-image-name> should be the name, given before
in step 3.
One important options is to set the directories: <dir-to-add>:<where-to-add>. This allows you to add specific
directories, that are needed within yolo into the container.
To run a Docker Image there are different [options](https://docs.docker.com/engine/reference/commandline/run/).

```bash
docker run -v <dir-to-add>:<where-to-add> --shm-size 8 --name <any-name> --gpus all -it -p 8070:8070 <docker-image-name>

# Example:
docker run -v /<path-to-folder>/weights/:/darknet/weights -v /<path-to-folder>/config/:/darknet/config -v /<path-to-folder>/datasets/:/darknet/datasets -v /<path-to-folder>/inference/:/darknet/inference -v /<path-to-folder>/prediction/:/darknet/prediction --shm-size 8 --name yolo4_tegel --gpus all -it -p 8070:8070 berlin-txl-inference
```

After running the container, you can check with `docker ps -a` if the container is alive.


### Yolo v4 (Darknet)

YOLOv4 is a state-of-the-art object detection algorithm that was released in 2020. It stands for "You Only Look Once" version 4, and it uses a neural network architecture called Darknet to detect objects in images and video frames.
When setting up the docker container, yolov4 is automatically downloaded from github and installed inside the container.
No further steps are necessary!

## Usage

When we want to use our weights & YOLO to apply on pictures (e.g. create bounding boxes, .txt, .json),
we use the container in which everything is ready to use.

### Enter a running container:
```bash
docker container exec -it <name-of-container> bash
```

After enteringthe running container, the terminal change to something like this:
```bash
root@32a5c1afbb10:/darknet#
```
You can use `ls` to check the contains of this folder. If every step was conducted in the right way, there also should be
the '/inference' directory, which was added via the '<dir-to-add>:<where-to-add>' option.
In this directory, you will all necessary subfolders (containing configuration), as well as 3 scripts (for inference):

- *inference-txl-basic-bb-label.py*: create bounding boxes (small) and label files for training.
- *inference-txl-basic-bb-big-label.py*: create bounding boxes (orig. size) and label files.
- *inference-txl-blurring-mask-json.py*: blurr pictures, create masks and label files.
- *inference-txl-blurring-violet.py*: violet blurr to objects.

Inside each script, you can configure different variables (to switch on/off e.g. creating labels) or setting
the necessary directories for input pictures or the output destination.
After adapting the script to your wishes, you have to copy this script to the top-level of the 'darknet' folder:

```bash
# Example:
root@32a5c1afbb10:/darknet# cp inference/inference-txl-basic-bb-label.py .
```

The script, now at the top-level, can be run with following command:
```bash
# Example:
root@32a5c1afbb10:/darknet# python3 inference-txl-basic-bb-label.py
```

### Leave a container without stopping it:

```bash
<strg + p> <strg + q>
```



## Contributing

[yolov4 - Offical github repo](https://github.com/AlexeyAB/darknet)
