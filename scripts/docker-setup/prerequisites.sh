#!/bin/sh

apt-get update

# install some libraries we will need.
apt-get install -y libsm6 libxext6 libxrender-dev python3 python3-pip curl wget git vim tmux

python3 -m pip3 install -U --force-reinstall pip3

python3 -m pip install -U --force-reinstall pip

pip3 install --user tqdm pillow scikit-learn scipy scikit-image pandas opencv-python==4.2.0.34
