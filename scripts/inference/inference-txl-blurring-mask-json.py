# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


""" Main skript to create blurred images, (bw) masks and also labels. """


__version__ = "1.0"
__author__ = "klaus.moser@fujitsu.com"


import os
import darknet
import random
import cv2
import json
import time
import numpy as np
from PIL import Image
from PIL.ExifTags import TAGS
from tqdm import tqdm


# configuration files
config_file = '/darknet/config/yolov4-custom.cfg'  # configuration file
data_file = '/darknet/config/obj.data'  # class number
weights = '/darknet/weights/yolov4-training-09-best-weights-01-03-2023.weights'  # weights file

# Input
dirpath = '/darknet/datasets/EXAMPLE-INFERENCE-SET'  # input images

# local configuration
conf_thresh = 0.30  # threshold of detection for a class
Dict_labels = {'bus': 0, 'car': 1, 'person': 2, 'lkw': 3, 'construction': 4, 'bike': 5}
random.seed(3)  # deterministic bbox colors

# choices
WRITE_LABEL = True
SAVE_BLURRED_IMAGE = True
CREATE_MASK = True


fps_detector = []
fps_preprocessing = []
statistics = []

network, class_names, class_colors = darknet.load_network(
    config_file,
    data_file,
    weights,
    batch_size=1
)


def image_detection(image_path):
    """
    This function loads the image and then returns the probability of an object
    and the images with the respective bounding boxes surrounding the object.

    :return:
    """

    path = image_path.split('.')[0]
    suffix = image_path.split('.')[1]


    start = time.time()

    # Darknet doesn't accept numpy images. Create acceptable image format for darknet.
    width = darknet.network_width(network)
    height = darknet.network_height(network)
    darknet_image = darknet.make_image(width, height, 3)

    im = Image.open(image_path)  # open image with PIL
    EXIF = im.getexif()  # extract metadata from image
    im.close()  # close image

    image = cv2.imread(image_path)
    height_orig, width_orig, channels = image.shape

    # factor to recalculate
    width_factor = width_orig / width  # 8192
    height_factor = height_orig / height  # 5460

    # preprocessing the given input image
    preproc_time = time.time()
    image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    # resize the images with respecting the ratio
    image_resized = cv2.resize(image_rgb, (width, height), interpolation=cv2.INTER_LINEAR)
    darknet.copy_image_from_bytes(darknet_image, image_resized.tobytes())
    fps_preprocessing.append(1 / (time.time() - preproc_time))

    # find the detection and draw bounding box arround the detected object.
    detections = darknet.detect_image(network, class_names, darknet_image, thresh=conf_thresh)

    print("start of inference: ")

    detections_new = []

    for d in detections:
        n = (
        d[0], d[1], (d[2][0] * width_factor, d[2][1] * height_factor, d[2][2] * width_factor, d[2][3] * height_factor))

        detections_new.append(n)

    end = time.time()
    duration = end - start
    statistics.append(duration)

    print("inference time: ", duration)

    # corners array
    corners = list()
    num = 0
    mask = np.ones((height_orig, width_orig, 3), dtype=np.uint8) * 255

    for label, confidence, bbox in detections:

        # safety to not go out of bounds
        for i in range(len(bbox)):
            if bbox[i] < 0: bbox[i] = 0

        # Ending: represents the top left corner of rectangle
        end_x = int((bbox[0] * width_factor) - ((bbox[2] * width_factor) / 2))
        end_y = int((bbox[1] * height_factor) - ((bbox[3] * height_factor) / 2))

        # safety to not go out of bounds
        if end_x < 0: end_x = 0
        if end_y < 0: end_y = 0

        end_point = (end_x, end_y)

        # Blurring
        left = end_point[0]
        top = end_point[1]

        size_x = int(bbox[2] * width_factor)
        size_y = int(bbox[3] * height_factor)

        right = size_x + left
        bottom = size_y + top
        corners.append((num, label, left, top, right, bottom))
        num += 1

        # Blurring
        kernel = 90  # The higher the number the stronger the blurr

        blurred_part = cv2.blur(image_rgb[top:top + size_y, left:left + size_x], ksize=(kernel, kernel), )
        image_rgb[top:top + size_y, left:left + size_x] = blurred_part
        image_blurred = image_rgb

        # mask
        white = (255, 255, 255)
        black = (0, 0, 0,)

        # the additional corners are stored in variables x1, y1, x2, y2
        cv2.rectangle(mask, (left, top), (right, bottom), black, -1)

    # save mask
    if CREATE_MASK:
        cv2.imwrite(path + "_MASK." + suffix, mask)

    # save .json
    if WRITE_LABEL:
        json_obj = {}
        # Iterate through the list of tuples
        for tup in corners:
            # The first element of the tuple is the key, and the rest of the elements are the value
            key = tup[0]
            value = tup[1:]
            # Add the key-value pair to the JSON object
            json_obj[key] = value

        # Use the json.dumps() method to convert the JSON object to a string
        json_str = json.dumps(json_obj)

        # write the json string to file
        with open(path + '.json', 'w') as f:
            json.dump(json_obj, f)


    # create a blurred image
    if SAVE_BLURRED_IMAGE:
        pil_image=Image.fromarray(image_rgb)
        image_path = path + '_BLURRED.' + suffix
        pil_image.save(image_path, format=im.format, exif=EXIF, quality=95)  # 'keep'


list_all_files = []
for root, dirs, files in os.walk(dirpath):
    for file in files:
        if not file.endswith('JPG'):
            continue
        list_all_files.append(os.path.join(root, file))


for index in tqdm(range(len(list_all_files))):
    image_detection(list_all_files[index])  # file-per-file

print(f"mean value of all the durations of the inference: {sum(statistics) / len(statistics)}")
