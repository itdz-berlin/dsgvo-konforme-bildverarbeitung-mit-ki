# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


""" Main script to create bounding-boxes and labels. """


__version__ = "1.0"
__author__ = "klaus.moser@fujitsu.com"


import numpy as np
import os
import darknet
import random
import cv2
import pandas as pd
from tqdm import tqdm
import time
import sys
import shutil
import pwd
import grp


# configuration files
config_file = '/darknet/config/yolov4-custom.cfg'  # configuration file
data_file = '/darknet/config/obj.data'  # class number
weights = '/darknet/weights/yolov4-training-09-best-weights-01-03-2023.weights'  # weights file

# source & destination
dirpath = '/darknet/datasets/EXAMPLE-INFERENCE-SET'  # input images
out_folder_bb = '/darknet/prediction/bb'  # output of bounding boxes
out_folder_labels = '/darknet/prediction/labels'  # output of labels

# choices
WRITE_LABEL = True  # create labels
SAVE_IMAGE = True  # create bounding boxes
MOVE_OBSOLETE = False  # move images without objects to '<dirpath>_obsolete'
COMBINE = False  # copy labels into the 'dirpath' folder

# local configuration
conf_thresh = 0.30  # threshold of detection for a class
Dict_labels = {'bus': 0, 'car': 1, 'person': 2, 'lkw': 3, 'construction': 4, 'bike': 5}
random.seed(3)  # deterministic bbox colors

network, class_names, class_colors = darknet.load_network(
    config_file,
    data_file,
    weights,
    batch_size=1
)

image_paths = os.listdir(dirpath)

fps_detector = []
fps_preprocessing = []
statistics = []


def image_detection(image_path, save_path_labels, save_path_images_with_bb):
    """
    This function loads the image and then returns the probability of an object
    and the images with the respective bounding boxes surrounding the object.

    :return:
    """

    start = time.time()
    # Darknet doesn't accept numpy images. Create acceptable image format for darknet.
    width = darknet.network_width(network)
    height = darknet.network_height(network)
    darknet_image = darknet.make_image(width, height, 3)


    image = cv2.imread(image_path)

    # preprocessing the given input image
    preproc_time = time.time()
    image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    # resize the images with respecting the ratio
    image_resized = cv2.resize(image_rgb, (width, height), interpolation=cv2.INTER_LINEAR)
    darknet.copy_image_from_bytes(darknet_image, image_resized.tobytes())
    fps_preprocessing.append(1/(time.time() - preproc_time))

    # find the detection and draw bounding box arround the detected object.
    detections = darknet.detect_image(network, class_names, darknet_image, thresh=conf_thresh)
    text_file_name = os.path.splitext(save_path_labels)[0] + ".txt"

    # Save label --------------------------------
    if detections and WRITE_LABEL:
        with open(text_file_name, "w") as f:
            for label, confidence, bbox in detections:
                label = Dict_labels.get(label)

                # save label
                f.write("{} {:.4f} {:.4f} {:.4f} {:.4f}\n".format(label, bbox[0]/width, bbox[1]/height, bbox[2]/width, bbox[3]/height))


    print("start of inference: ")

    image_with_bounding_boxes = darknet.draw_boxes(detections, image_resized, class_colors)
    end = time.time()
    duration = end - start
    statistics.append(duration)

    print("inference time: ", duration)


    # Move 'empty' pictures (with no detections) to a specific folder
    if not detections and MOVE_OBSOLETE:
        try:
            shutil.move(image_path, obsolete_pics_folder)
        except:
            with open(os.path.join(obsolete_pics_folder, "log.txt"), 'a' ,encoding = 'utf-8') as f:
                f.write(image_path + "\n")

    else:
        # Save image --------------------------------
        if SAVE_IMAGE:
            cv2.imwrite(save_path_images_with_bb, cv2.cvtColor(image_with_bounding_boxes, cv2.COLOR_RGB2BGR))


# Move 'empty' pictures (with no detections) to a specific folder
if MOVE_OBSOLETE:
    obsolete_pics_folder = dirpath + "_obsolete"

    try:
        os.mkdir(obsolete_pics_folder)
        os.chown(obsolete_pics_folder, uid=1000, gid=1000)  # change ownership
    except Exception as e:
        print(e)

for index in tqdm(range(len(image_paths))):
    if not image_paths[index].endswith("JPG"):
        print("Remove not JPG file: ", image_paths[index])
        try:
            os.remove(os.path.join(dirpath, image_paths[index]))
        except:
            print("Could not remove file: ", image_paths[index])

        continue
    else:
        print(image_paths[index])
        image_detection(os.path.join(dirpath, image_paths[index]), os.path.join(out_folder_labels, image_paths[index]), os.path.join(out_folder_bb, image_paths[index]))


if statistics:
    print("mean value of all the durations of the inference", sum(statistics) / len(statistics))

# combine:
# Copy the created labels to the input images. This is usefull when using this
# data as new input for training, where .JPG/.txt pairs are necessary.
if COMBINE:
    for label in os.listdir(out_folder_labels):
        src = os.path.join(out_folder_labels, label)
        dst= os.path.join(dirpath, label)
        try:
            shutil.copy(src, dst)
        except Exception as e:
            print(e)
